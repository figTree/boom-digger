from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scatterlayout import ScatterLayout


class Box(BoxLayout):

    
    def on_touch_down(self, touch):
        if len(self.children) > 1:
            if self.children[1].collide_point(*touch.pos): # Dispatch to header only
                self.children[1].on_touch_down(touch)
            else:
                super().on_touch_down(touch)

        else:
            super().on_touch_down(touch)

    def on_touch_move(self, touch):
        super().on_touch_move(touch)

class Scatter(ScatterLayout):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.trans = False

    def on_touch_move(self, touch):
        if not self.trans:
            if touch.time_update - touch.time_start > 0.15:
                self.trans = True
        super().on_touch_move(touch)
    
    def transform_with_touch(self, touch):
        if self.trans:
            super().transform_with_touch(touch)
        if self.scale > 1:
            self.do_translation = True
        else:
            self.do_translation = False

    def on_touch_up(self, touch):
        if self.trans:
            self.trans = False
            for i in self.content.children:
                i.firstClick = True 
            if self.scale == 1:
                self.pos = self.content.children[0].scStartPos

        super().on_touch_up(touch)

