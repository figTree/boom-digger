#!/usr/bin/python3

from kivy.lang import Builder
from kivy.core.audio import SoundLoader

from reflabel import RefLabel

from random import randint
from threading import Thread



class BoomDigger(RefLabel):
    menu_options = ((8, 8, 10, 40), (16, 16, 50, 30))
    colors = ("0950f4", "00c825", "ff1000", "d314d0", "e30095", "00d9bf", "631c1c", "ffffff")

    def __init__(self, header, layout, scatter, top=None, **kwargs):
        super().__init__(**kwargs)

        self.bheader = header
        self.layout = layout
        self.scatter = scatter

        self.bsound = SoundLoader.load('sound/boom.ogg')

        self.wsound = SoundLoader.load('sound/win.ogg')
        self.wsound.volume = 0.5
        
        self.selsound = SoundLoader.load('sound/select.ogg')
        self.selsound.volume = 0.1

        self.ssound = SoundLoader.load('sound/whine.ogg')
        self.ssound.volume = 0.1


        self.menu = True
        self.boom = False
        self.center_items = True

        # Mitigation for on_touch_up issue after menu selection
        # Running out of time ....
        # Need ...
        # food .....
        # internet bill
        # AHHH!
        self.firstClick = True

        self.flag_character = "F"

        self.space = 3

        self.scStartPos = (0, 0)
        self.position = None

        # Large split
        self.btop = top
        self.bbottom = None

    def reset(self, rows=6, cols=6, bombs=10, font=40):
        if self.menu:
            self.rows = rows
            self.cols = cols
            self.bomb_count = bombs
            self.font_name = "font/boom"
            self.font_size = "{}sp".format(font)
            self.spacer = ' '

            if not self.btop:
                self.bheader.reset(flags=bombs)
                self.layout.add_widget(self.bheader)
                self.ssound.play()
            else:
                self.bheader.add_flags(bombs)
        else:
            if not self.btop:
                self.bheader.reset()

            self.scatter.scale = 1.0
            self.scatter.pos = self.scStartPos
            
        self.scatter.do_scale=True

        self.header.clear()
        self.color = [0.81, 0.71, 0, 0.8]
        self.references = True
        self.bombs = self.bomb_scatter(self.bomb_count)
        self.flags = []
        self.boom = False
        self.bheader.flagMode = False
        self.items.clear()

        self.init()

    def bomb_scatter(self, number):
        bombs = []
        bomb = randint(0, self.spaces() - 1)
        space = self.spaces() - 1

        for i in range(number):
            while bomb in bombs:
                bomb = randint(0, space)
            bombs.append(bomb)

        bombs.sort()
        return bombs
    
    def counter(self, lst, scope='bomb'):
        if type(lst) == int:
            lst = self.get_circle(lst)

        if scope == 'bomb':
            check = self.bombs
        elif scope == 'flags':
            check = self.flags


        c = 0
        for i in lst: 
            if i in check:
                c += 1

        return c

    def scanner(self, lst, boom=False):
        checked = []
        top = []
        bottom = []

        while lst:
            check = lst[:]
            for item in check:
                if item in checked or item in self.flags:
                    lst.remove(item)
                    continue
                elif item in self.bombs:
                    if boom:
                        self.gameover('lose', item)
                        if self.btop:
                            self.btop.gameover('lose')
                        elif self.bbottom:
                            self.bbottom.gameover('lose')
                        return
                    else:
                        lst.remove(item)
                        continue

                count = self.counter(item)

                if self.btop:
                    if item in range(24):
                        line = self.btop.get_cross(item + 264, count=(2,1))
                        count += self.btop.counter(line)

                        if count == 0:
                            for i in line:
                                if i not in top:
                                    if self.btop.items[i] == self.filler:
                                        top.append(i)

                elif self.bbottom:
                    if item in range(264, 288):
                        line = self.bbottom.get_cross(item - 264, count=(2,1))
                        count += self.bbottom.counter(line)

                        if count == 0:
                            for i in line:
                                if i not in bottom:
                                    if self.bbottom.items[i] == self.filler:
                                        bottom.append(i)

                if count == 0:
                    self.items[item] = '[ref=None][color=8d8d8d]b[/color][/ref]'
                    for i in self.get_circle(item):
                        if i not in checked:
                            lst.append(i)
                else:
                    self.items[item] = '[color={}]{}[/color]'.format(BoomDigger.colors[count - 1], count)
                
                checked.append(item)

        if top:
            Thread(target=self.btop.scanner, args=[top, boom]).start()
        elif bottom:
            Thread(target=self.bbottom.scanner, args=[bottom, boom]).start()

        self.init()

    def blanker(self, indicieList):
        for indicie in indicieList:
            self.items[indicie] = '[ref=None][color=8d8d8d]b[/color][/ref]'

    def box_check(self, indicie):
        checks = self.get_circle(indicie)

        count = self.counter(checks)

        # Include second board from large
        if self.btop:
            if indicie in range(24):
                count += self.btop.counter(\
                        self.btop.get_cross(indicie + 264, count=(2,1)))

        elif self.bbottom:
            if indicie in range(264, 288):
                count += self.bbottom.counter(\
                        self.bbottom.get_cross(indicie - 264, count=(2,1)))

        if count > 0:
            self.items[indicie] = '[color={}]{}[/color]'.format(BoomDigger.colors[count - 1], count)
            self.init()

        else:
            self.items[indicie] = '[ref=None][color=8d8d8d]b[/color][/ref]'
            self.scanner(self.get_circle(indicie, fill=False))

    def gameover(self, state, indicie=None):
        self.boom = True
        self.references = False
        self.firstClick = True
        if not self.btop:
            self.bheader.stop_time()
            self.scatter.scale = 1
            self.scatter.pos = self.scStartPos
            self.scatter.do_scale = False
            self.scatter.do_translation = False

        if state == 'lose':
            if indicie != None:
                self.items[indicie] = '[color=f40003]E[/color]'
                self.bombs.remove(indicie)

            for ind in self.bombs: # Bomb Reveal
                if ind not in self.flags:
                    self.items[ind] = "[color=ffffff]E[/color]"

            for ind in self.flags: # Reveal mistakes
                if ind not in self.bombs:
                    self.items[ind] = 'X'

            if not self.btop:
                self.bheader.death()
                self.bsound.play()

        elif state == 'win':
            if not self.btop:
                self.bheader.win()
                self.wsound.play()

        # Give buttons for reset and menu
        if not self.btop:
            self.header.extend(["[color=ffffff][ref=menu][font=Roboto]Menu       [/ref]", "[ref=reset]    New Game[/ref][/font][/color]"])

        self.init()

    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):

            if self.boom:
                super(RefLabel, self).on_touch_down(touch)
                return

            if not self.menu and not self.bheader.flagMode:
                self.bheader.careful()

            else:
                super(RefLabel, self).on_touch_down(touch)

    def on_touch_up(self, touch):
        if self.collide_point(*touch.pos):
            if not self.menu and not self.boom:

                if not self.bheader.flagMode:
                    self.bheader.safe()
                    if self.firstClick:
                        self.firstClick = False
                        if self.bbottom:
                            self.bbottom.firstClick = False
                        elif self.btop:
                            self.btop.firstClick = False
                    else:
                        self.selsound.play()
                        super().on_touch_down(touch)

    def on_selection(self, instance, name):
        if not self.menu and not self.boom:
            if name: # Double click deselects and sends us an empty selection
                con = eval(name[0])

                obj = con[0]
                indicie = con[1]

                if obj != 'B' and indicie  not in self.flags: # Scanning from bomb count

                    # Amount of flags matches amount of bombs
                    counter = self.counter(indicie,  scope='flags')
                    line = []

                    if self.btop:
                        if indicie in range(24):
                            line = self.btop.get_cross(indicie + 264, count=(2,1))
                            counter += self.btop.counter(line,  scope='flags')
                    elif self.bbottom:
                        if indicie in range(264, 288):
                            line = self.bbottom.get_cross(indicie - 264, count=(2,1))
                            counter += self.bbottom.counter(line,  scope='flags')

                    if counter == int(obj):

                        if self.btop:
                            if line:
                                Thread(target=self.btop.scanner, args=[line, True]).start()
                        elif self.bbottom:
                            if line:
                                Thread(target=self.bbottom.scanner, args=[line, True]).start()


                        check = []

                        for i in self.get_circle(indicie, fill=False):
                            if self.items[i] == self.filler:
                                check.append(i)

                        if check:
                            self.scanner(check, boom=True)

                else: # Not a bomb count
                    super().on_selection(instance, name)

            else: # Double click
                super().on_selection(instance, name)

        else: # Inside menu
            super().on_selection(instance, name)

    def on_selected(self, instance, name):
        indicie = name[0]

        if not self.menu:
            if indicie == 'reset':

                if self.bbottom: # top has reset button
                    self.bbottom.reset()

                self.reset()
                return
            
            elif indicie == 'menu':
                # Reset back to Menu state
                if self.bbottom: # top has button
                    self.scatter.remove_widget(self.bbottom)
                    del self.bbottom
                    self.bbottom = None

                self.menu = True
                self.references = True
                self.line_height = 1
                self.size_hint = (1, 1)

                self.layout.remove_widget(self.bheader)
                
                self.canvas.clear()
                Builder.apply(self)

                return

            if self.bheader.flagMode:
                # Flag only flags and boxes
                if self.items[indicie] == self.filler or self.items[indicie] == self.flag_character:

                    if indicie in self.flags: # Remove flag
                        self.flags.remove(indicie)
                        self.items[indicie] = self.filler
                        self.bheader.add_flag()

                    else: # Add Flag
                        if self.bheader.flag_count > 0:
                            self.flags.append(indicie)
                            self.items[indicie] = self.flag_character
                            self.bheader.remove_flag()

            else: # select box
                if indicie not in self.flags:

                    if indicie in self.bombs:
                        self.gameover('lose', indicie)
                        if self.btop:
                            self.btop.gameover('lose')
                        elif self.bbottom:
                            self.bbottom.gameover('lose')

                    else:
                        self.box_check(indicie)


            if not self.boom: # Game is on-going

                if self.bheader.flag_count == 0: # Check for win
                    self.flags.sort()

                    # Check all boards
                    if self.btop:
                        self.btop.flags.sort()
                        if self.btop.flags == self.btop.bombs:
                            if self.flags == self.bombs:
                                self.gameover('win')
                                self.btop.gameover('win')

                    elif self.bbottom:
                        self.bbottom.flags.sort()
                        if self.bbottom.flags == self.bbottom.bombs:
                            if self.flags == self.bombs:
                                self.bbottom.gameover('win')
                                self.gameover('win')

                    else:
                        if self.flags == self.bombs:
                            self.gameover('win')

                self.init()

        else: # In menu
            if indicie == 'about':
                self.references = False
                self.header[0] += '[size=20]V 0.2[/size]\n'

                self.items.clear()
                self.items.append("Programmed in python using reflabel, an extension to Kivy's label class.\n\n")
                self.items.append(u"Programmer: Jesse K. Lapere\nCopyright\xa9 2019 Fig Tree Productions")
                self.items.append("Released under the GPL v3 Licence. Source can be located at: \n www.gitlab.com/figTree/boom-digger\n")
                self.items.append("[ref=menu]Back[/ref]")
                
                self.init()

            elif indicie == 'menu':
                self.header[0] = "BOOM DIGGER\n\n\n\n"
                self.references = True
                self.items.clear()
                self.canvas.clear()
                Builder.apply(self)

            elif indicie == 2:
                # Split large across two boards for speed
                self.bbottom = BoomDigger(self.bheader, self.layout, self.scatter,\
                        self, default=False, size_hint=(1, 0.5), \
                        pos_hint={'top': 0.5}, valign='top')
                        


                self.bbottom.header.clear()

                 #Bring the two boards together
                self.size_hint = (1, 0.5)
                self.pos_hint = {'top': 1}
                self.valign = 'bottom'


                self.reset(12, 24, 50, 20)
                self.bbottom.reset(12, 24, 49, 20)

                self.scatter.remove_widget(self)

                self.scatter.add_widget(self.bbottom)
                self.scatter.add_widget(self)

                self.menu = False
                self.bbottom.menu = False

            else:
                self.reset(*BoomDigger.menu_options[indicie])
                self.menu = False
