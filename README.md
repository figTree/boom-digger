#BOOM DIGGER

You find yourself in an immense jungle. Your spectra-shovel hums to life and illuminates the
ground before you with the faint glow of the setting sun. This can only mean one thing.

Will you be able to use your tool to get through the bush to the other side? Or will the 
arpeggio be drowned out by the long growl of the explosion you will never get to hear?


##How to play:

In selecting a box, your spectra-shovel will hum as it scans the area. If there are bombs
near the vicinity, your tool will tell you how many. Get too close to one however, and your
doom awaits. 

To win the game, select the face at the top of the board to switch to marker mode, and mark
all areas suspected of containing bombs. If you are correct, you reign as champion, and the
game will end. How fast can you hop through the bomb laced thicket?

*hint*
In both seek and marker mode, if a bomb count has the correct amount of markers, selecting
it will scan the entire area around the count. Be warned however that any mistakes will
have immediate consequences.


## Requirements:

* python3
* kivy
* reflabel (release TBA)


## Start-up

Run the *main.py* executable.
