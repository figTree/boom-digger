#!/usr/bin/python3
from kivy.config import Config
Config.set('input', 'mouse', 'mouse,multitouch_on_demand')

from kivy.app import App

from layout import *
from header import BoomHeader
from boomdigger import BoomDigger



from kivy.uix.label import Label

class MinesApp(App):

    def build(self):
        layout = Box(orientation='vertical')
        mlayout = Scatter(do_rotation=False,\
                do_translation=False, do_scale=False,\
                scale_min=1)

        header = BoomHeader(size_hint=(1, 0.07))

        mine = BoomDigger(header, layout, mlayout, size_hint=(1, 0.8))

        mine.init()

        mlayout.add_widget(mine)

        layout.add_widget(mlayout)


        return layout



if __name__ == '__main__':
    test = MinesApp()
    test.run()
else:
    test = BoomDigger()

