#!/usr/bin/python3

from kivy.properties import NumericProperty
from kivy.core.audio import SoundLoader

from reflabel import RefLabel


# Seperated header for Boom Digger Android


class BoomHeader(RefLabel):

    def __init__(self, **kwargs):
        super().__init__(set_label=False, **kwargs)

        self.font_name = 'boxTest'
        self.font_size = '40sp'

        self.cols = 5
        self.rows = 1
        
        self.references = False
        self.highlight = False

        self.flagMode = False
        self.flag_count = 0
        self.flags = 0
        self.space = " " * 3

        self.tsound = SoundLoader.load('sound/tick.ogg')

        self.items = ["[font=Roboto][color=ca0909]{}[/color][/font]".format(self.flag_count), self.space, "[color=fef400][ref=flag]s[/ref][/color]", self.space, "[font=Roboto][color=ca0909] 000[/color][/font]"]

    def reset(self, flags=None):
        if flags:
            self.flag_count = flags
            self.flags = flags
        else:
            self.flag_count = self.flags

        self.update_flags()
        self.timer = 0
        self.safe()
        self.start_time()

    def stop_time(self):
        self.time.cancel()
        del self.Ttimer

    def start_time(self):
        self.timer = 0
        self.time = self.ticker('timer')

    def death(self):
        self.items[2] = "D"
        self.init()

    def win(self):
        self.items[2] = "[color=ff7afa]W[/color]"
        self.init()

    def careful(self):
        self.items[2] = "[color=fef400]S[/color]"
        self.init()

    def safe(self):
        self.items[2] = "[color=fef400][ref=flag]s[/ref][/color]"
        self.init()

    def on_timer(self, *args):
        self.items[4] ="[font=Roboto][color=ca0909] {}[/color][/font]".format(str(self.timer).zfill(3)) 
        self.init()
        self.tsound.play()

    def flag_mode(self):
        if self.flagMode:
            self.flagMode = False
            self.safe()

        else:
            self.items[2] = "[ref=flag]F[/ref]"
            self.flagMode = True

            self.init()

    def add_flags(self, amount):
        self.flag_count += amount
        self.flags += amount
        self.items[0] = "[font=Roboto][color=ca0909]{}[/color][/font]".format(str(self.flag_count).zfill(2))
        self.init()

    def remove_flags(self, amount):
        self.flag_count -= amount
        self.flags -= amount
        self.items[0] = "[font=Roboto][color=ca0909]{}[/color][/font]".format(str(self.flag_count).zfill(2))
        self.init()

    def add_flag(self):
        self.flag_count += 1
        self.items[0] = "[font=Roboto][color=ca0909]{}[/color][/font]".format(str(self.flag_count).zfill(2))
        self.init()

    def remove_flag(self):
        self.flag_count -= 1
        self.items[0] = "[font=Roboto][color=ca0909]{}[/color][/font]".format(str(self.flag_count).zfill(2))

        self.init()

    def update_flags(self):
        self.items[0] = "[font=Roboto][color=ca0909]{}[/color][/font]".format(str(self.flag_count).zfill(2))

    def on_selected(self, instance, name):
        if name:
            self.flag_mode()
            self.selection.clear()



    timer = NumericProperty(0)
